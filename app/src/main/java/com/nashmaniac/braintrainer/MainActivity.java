package com.nashmaniac.braintrainer;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Button goButton;
    RelativeLayout gameLayout;
    TextView timeView;
    TextView scoreView;
    Button playAgain;
    TextView resultView;
    TextView questionView;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    int locationOfCorrectAnswer;
    int answersCount = 0;
    int QuestionCount = 0;
    ArrayList<Integer> answers = new ArrayList<Integer>();
    public void generateQuestion(){
        Random rand = new Random();
        int a = rand.nextInt(25);
        int b = rand.nextInt(25);
        questionView.setText(Integer.toString(a)+" + "+Integer.toString(b));

        locationOfCorrectAnswer = rand.nextInt(4);
        answers.clear();
        int incorrectAnswer;
        for(int i=0;i<4;i++){
            if(i==locationOfCorrectAnswer){
                answers.add((a+b));
            }else{
                incorrectAnswer = rand.nextInt(51);
                while (incorrectAnswer == (a+b)){
                    incorrectAnswer = rand.nextInt(51);
                }
                answers.add(incorrectAnswer);
            }
        }
        button0.setText(Integer.toString(answers.get(0)));
        button1.setText(Integer.toString(answers.get(1)));
        button2.setText(Integer.toString(answers.get(2)));
        button3.setText(Integer.toString(answers.get(3)));
        scoreView.setText(Integer.toString(answersCount)+"/"+Integer.toString(QuestionCount));
    }
    public void chooseAnswer(View view){
        Button chosenButton = (Button) view;
        Log.i("Button Pressed", chosenButton.getTag().toString());
        if(Integer.parseInt(chosenButton.getTag().toString())==locationOfCorrectAnswer){
            resultView.setText("Correct!");
            answersCount++;
        }else{
            resultView.setText("Incorrect");
        }
        QuestionCount++;
        scoreView.setText(Integer.toString(answersCount)+"/"+Integer.toString(QuestionCount));
        generateQuestion();
    }
    public void gamePlay(){
        generateQuestion();
        new CountDownTimer(30100, 1000){
            @Override
            public void onTick(long l) {
                int time_left = ((int) l/1000);
                timeView.setText(Integer.toString(time_left)+'s');
            };

            @Override
            public void onFinish() {
                timeView.setText("0s");
                resultView.setVisibility(View.VISIBLE);
                playAgain.setVisibility(View.VISIBLE);
                resultView.setText("You scored "+Integer.toString(answersCount)+"/"+Integer.toString(QuestionCount));
            }
        }.start();
    }
    public void gameplayAgain(View view){
        timeView.setText("30s");
        scoreView.setText("0/0");
        answersCount = 0;
        QuestionCount =0;
        resultView.setText("");
        scoreView.setText(Integer.toString(answersCount)+"/"+Integer.toString(QuestionCount));
        playAgain.setVisibility(View.INVISIBLE);
        gamePlay();
    }
    public void startGame(View view) {
        goButton.setVisibility(View.INVISIBLE);
        gameLayout.setVisibility(View.VISIBLE);
        gamePlay();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        goButton = (Button) findViewById(R.id.go_button);
        gameLayout = (RelativeLayout) findViewById(R.id.gameLayout);
        gameLayout.setVisibility(View.INVISIBLE);
        timeView = (TextView) (findViewById(R.id.timeView));
        scoreView = (TextView) findViewById(R.id.scoreView);
        playAgain = (Button) findViewById(R.id.playAgain);
        resultView = (TextView) findViewById(R.id.resultView);
        questionView = (TextView) findViewById(R.id.questionView);
        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
